# Generated by Django 4.0.3 on 2022-11-01 23:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0014_automobilevo_sold'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmployeeUserVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('employee_id', models.PositiveIntegerField(unique=True)),
                ('first_name', models.CharField(blank=True, max_length=150)),
                ('last_name', models.CharField(blank=True, max_length=150)),
                ('sale_employee', models.BooleanField(default=False)),
                ('service_employee', models.BooleanField(default=False)),
            ],
        ),
    ]
