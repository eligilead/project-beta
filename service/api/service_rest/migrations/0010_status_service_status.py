# Generated by Django 4.0.3 on 2022-10-27 16:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0009_service_time'),
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.PositiveSmallIntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=10)),
            ],
            options={
                'verbose_name_plural': 'statuses',
                'ordering': ('id',),
            },
        ),
        migrations.AddField(
            model_name='service',
            name='status',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='stauts', to='service_rest.status'),
        ),
    ]
