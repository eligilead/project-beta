from django.shortcuts import render
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import AutomobileVO, Customer, Technician, Service
from .encoders import ServiceListEncoder, ServiceDetailEncoder, CustomerListEncoder, CustomerDetailEncoder, TechnicianListEncoder, TechnicianDetailEncoder, AutomobileVODetailEncoder, AutomobileVOListEncoder
# Create your views here.







@require_http_methods(["GET", "POST"])
def api_list_service(request):
    if request.method == "GET":
        services = Service.objects.all()
        return JsonResponse(
            {"services": services},
            ServiceListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            employee_number = content["technician"]
            technician = Technician.objects.get(employee_number=employee_number)
            content["technician"] = technician


        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "technician does not exist"},
                status=404,
            )
        try:

            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "automobile does not exist"},
                status=404,
            )
        try:

            customer_id = content["customer"]
            customer = Customer.objects.get(customer_id=customer_id)
            content["customer"] = customer

        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404,
            )
        service = Service.create(**content)

        return JsonResponse(
            service,
            encoder= ServiceDetailEncoder,
            safe=False,
        )
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_service(request, pk):
    if request.method == "GET":
        service = Service.objects.get(id=pk)
        return JsonResponse(
            service,
            encoder=ServiceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Service.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:

            customer_id = content["customer"]
            customer = Customer.objects.get(pk=customer_id)
            content["customer"] = customer

            employee_number = content["technician"]
            technician = Technician.objects.get(pk=employee_number)
            content["technician"] = technician

            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile
        except (Technician.DoesNotExist, Customer.DoesNotExist, AutomobileVO.DoesNotExist):
            return JsonResponse(
                {"message": "Invalid does not exist"},
                status=404,
            )
        Service.objects.filter(id=pk).update(**content)
        service = Service.objects.get(id=pk)
        return JsonResponse(
            service,
            encoder=ServiceDetailEncoder,
            safe=False,
        )
@require_http_methods(["PUT"])
def api_finish_service_status(request, pk):

    service = Service.objects.get(id=pk)
    service.finish()
    return JsonResponse(
        service,
        encoder=ServiceDetailEncoder,
        safe=False,
    )
@require_http_methods(["PUT"])
def api_cancel_service_status(request, pk):
    service = Service.objects.get(id=pk)
    service.cancel()
    return JsonResponse(
        service,
        encoder=ServiceDetailEncoder,
        safe=False,
    )

@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            CustomerListEncoder,
        )
    else :
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)

        return JsonResponse(
            customer,
            encoder= CustomerDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(customer_id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(customer_id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(customer_id=pk).update(**content)
        customers = Customer.objects.get(customer_id=pk)
        return JsonResponse(
            customers,
            encoder= CustomerDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            TechnicianListEncoder,
        )
    else :
        content = json.loads(request.body)

        try:
            technicians= Technician.objects.get(employee_number=content["employee_number"])
            return JsonResponse({"message": "employee number in use"},
                    status=400,
                )

        except:
            technicians = Technician.objects.create(**content)


            return JsonResponse(
                technicians,
                encoder= TechnicianDetailEncoder,
                safe=False,
            )
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_technician(request, pk):
    if request.method == "GET":
        technicians = Technician.objects.get(employee_number=pk)
        return JsonResponse(
            technicians,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(employee_number=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Technician.objects.filter(employee_number=pk).update(**content)
        technicians = Technician.objects.get(employee_number=pk)
        return JsonResponse(
            technicians,
            encoder= TechnicianDetailEncoder,
            safe=False,
        )
