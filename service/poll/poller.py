import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here.
from service_rest.models import Customer, AutomobileVO, EmployeeUserVO


# def get_customers():
#     url= "http://sales-api:8000/customers/"
#     response = requests.get(url)
#     content = json.loads(response.content)
#     for customer in content["customers"]:
#         CustomerVO.objects.update_or_create(
#             href=customer["href"],
#             defaults={
#                 "full_name": customer["full_name"],
#                 "address": customer["address"],
#                 "phone_number": customer["phone_number"],
#             }
#         )

def get_automobiles():
    url = "http://inventory-api:8000/api/automobiles/"
    response = requests.get(url)

    content= json.loads(response.content)

    for auto in content["autos"]:
        AutomobileVO.objects.update_or_create(
            import_href=auto["href"],
            defaults={
                "color": auto["color"],
                "year": auto["year"],
                "vin": auto["vin"],
                "sold":auto["sold"],
            }

        )


def get_accounts():
    response = requests.get("http://accounts-api:8000/accounts/")
    content = json.loads(response.content)
    print("test---->", content)
    for accounts in content["employee_accounts"]:
        EmployeeUserVO.objects.update_or_create(
            employee_id=accounts["employee_id"],
            defaults={"first_name": accounts["first_name"],
            "last_name": accounts["last_name"],
            # "employee_id": accounts["employee_id"],
            "sale_employee": accounts["sale_employee"],
            "service_employee": accounts["service_employee"],
            }
        )

def poll():
    while True:
        print('Service poller polling for data')
        try:
            # Write your polling logic, here
            get_automobiles(),
            get_accounts()

        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(6)


if __name__ == "__main__":
    poll()
