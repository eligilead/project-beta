# CarCar

Team:

* Person 1 - Which microservice? Jonas completing the services microservice.

* Person 2 - Which microservice? Elijah completing the sales microservice.

## Design

![Domain Design](./assets/DD.png)
![Domain Design](./assets/React%20App%20-%20Project%20Beta%20Walkthrough.mp4)

## Service microservice
We decided to follow our domain design models once we read and talked about the scope of our project. Using docker and insomnia tools I was able to add the models and test the functionality of the website. With the use of a poller were able to synchronize data between the different microservices. This Single page application was built using restful api on the backend to help keep our project lightweight, flexible and scalable.

One particular feature I enjoyed adding was the ability to create a service ticket and have the status automatically show pending until you update the status with a cancel or finished button. Another well built feature is the ability to see the service history of a particular car by the vin number regarless if it has changed owners, we the users can see what was done and who worked on it.


## Sales microservice

Implementing the sale microservice started with considering the output necessary for a cohesive full stack application. I used Django models for Customer, SalePersons, and SalesRecords, with specific properties to represent their data. Additionally, using a polling function to acquire data from the Inventory Monolith’s Automobile model to populate the AutomobileVO with vin numbers, model names and sold data.

I also created a responsive frontend using Javascript and React, users have the ability to create customers, sales representatives, and sales records. Also, collaborated with my partner to create pages for Inventory Monolith.

Finally using Docker containers/ images my partner and I were able to scale our applications independently. Furthermore, my teamate and I used git and gitlab for verison control.
