import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import User
from .common.json import ModelEncoder


class UserListEncoder(ModelEncoder):
    model = User
    properties = [
        "employee_id",
        "first_name",
        "last_name",
        "sale_employee",
        "service_employee"]

class UserDetailEncoder(ModelEncoder):
    model = User
    properties = [
        "employee_id",
        "first_name",
        "last_name",
        "sale_employee",
        "service_employee",
        "email",
        "is_staff",
        "is_active",
        "date_joined",
        ]

@require_http_methods(["GET", "POST"])
def list_accounts(request, employee_id=None):
    if request.method == "GET":
        if employee_id is not None:
            employee_account = User.objects.filter(id=employee_id)
        else:
            employee_account = User.objects.all()

        return JsonResponse(
            {"employee_accounts": employee_account},
            encoder=UserListEncoder,
        )
    else:
        content = json.loads(request.body)
        username= f"{content['first_name']}_{content['last_name']}"
        content["username"] = username
        employee_account = User.objects.create(**content)
        return JsonResponse(
            employee_account,
            encoder=UserListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def detail_employee(request, employee_id):
    if request.method == "GET":
        employee_account = User.objects.get(id=employee_id)
        return JsonResponse(
            employee_account,
            encoder=UserDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = User.objects.filter(id=employee_id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            User.objects.filter(id=employee_id).update(**content)
            employee_account = User.objects.get(id=employee_id)
            return JsonResponse(
                employee_account,
                encoder=UserDetailEncoder,
                safe=False,
            )
        except User.DoesNotExist:
            return JsonResponse(
                {"message": "Employee does not exist"},
                status=400,
            )
