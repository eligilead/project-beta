from django.apps import AppConfig


class EmployeeAccountsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'employee_accounts'
