# Generated by Django 4.0.3 on 2022-11-02 15:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0014_remove_employeeuservo_is_active'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='salesrecord',
            name='sales_person',
        ),
        migrations.AddField(
            model_name='salesrecord',
            name='employee_user',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, related_name='emplpoyee_user', to='sales_rest.employeeuservo'),
            preserve_default=False,
        ),
    ]
