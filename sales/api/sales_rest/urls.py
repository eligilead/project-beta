from django.urls import path

from .views import (
    api_list_employees, 
    api_list_customer,
    api_detail_customer,
    api_list_sale_records,
    api_detail_sale_record,
    api_list_automobiles,
    )

urlpatterns = [
    #Get, Post
    path("employees/", api_list_employees, name="api_list_sale_person"),
    #Get, Delete, Update
    path("employees/<int:employee_id>/", api_list_employees, name="api_list_sale_person"),
    
    #Get, Post
    path("automobiles/", api_list_automobiles, name="api_list_automobiles"),
    #Get, Delete, Update
    path("automobiles/<int:pk>/", api_list_automobiles, name="api_list_automobiles"),
    
    #Get, Post
    path("customers/", api_list_customer, name="api_list_customer"),
    #Get, Delete, Update
    path("customers/<int:pk>/", api_detail_customer, name="api_detail_customer"),
    
    #Get, Post
    path("sales-records/", api_list_sale_records, name="api_list_sale_records"),
    #Get, Delete, Update
    path("sale-record/<int:pk>/", api_detail_sale_record, name="api_detail_sale_record"),
    

]
