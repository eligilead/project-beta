from django.contrib import admin
from .models import SalePerson, SalesRecord, Customer, AutomobileVO, EmployeeUserVO

@admin.register(SalePerson)
class SalePerson(admin.ModelAdmin):
    pass


@admin.register(SalesRecord)
class SalesRecord(admin.ModelAdmin):
    pass


@admin.register(Customer)
class Customer(admin.ModelAdmin):
    pass


@admin.register(AutomobileVO)
class AutomobileVO(admin.ModelAdmin):
    pass


@admin.register(EmployeeUserVO)
class EmployeeUserVO(admin.ModelAdmin):
    pass
