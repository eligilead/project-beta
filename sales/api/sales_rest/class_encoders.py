from common.json import ModelEncoder
from .models import SalePerson, SalesRecord, Customer, AutomobileVO, EmployeeUserVO


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["id", "vin", "model", "href", "sold"]


class SalePersonListEncoder(ModelEncoder):
    model = SalePerson
    properties = ["id", "full_name", "employee_id"]


class EmployeeUserVOListEncoder(ModelEncoder):
    model = EmployeeUserVO
    properties = [
        "id",
        "employee_id",
        "first_name",
        "last_name",
        "sale_employee",
        "service_employee"
        ]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "full_name", "phone_number"]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "full_name", 
        "phone_number",
        "address",
    ]

class SalesRecordListEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "employee_user",
        "customer",
        "automobile",
        "sales_price",
    ]
    def get_extra_data(self, o):
        return {
        "automobile": o.automobile.vin,
        "employee_user": f"{o.employee_user.first_name} {o.employee_user.last_name}",
        "employee_id": o.employee_user.employee_id,
        "customer": o.customer.full_name,
        }


class SalesRecordDetailEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "employee_user",
        "customer",
        "automobile",
        "sales_price",
    ]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "employee_user": EmployeeUserVOListEncoder(),
        "customer": CustomerDetailEncoder(),
    }
