from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import SalesRecord, Customer, AutomobileVO, EmployeeUserVO
from .class_encoders import (
    CustomerListEncoder,
    CustomerDetailEncoder,
    SalesRecordListEncoder,
    SalesRecordDetailEncoder,
    AutomobileVODetailEncoder,
    EmployeeUserVOListEncoder
)


@require_http_methods(["GET", "POST"])
def api_list_employees(request, employee_id=None):
    if request.method == "GET":
        if employee_id is not None:
            employee_user = EmployeeUserVO.objects.filter(id=employee_id)
        else:
            employee_user = EmployeeUserVO.objects.all()

        return JsonResponse(
            {"employee_users": employee_user},
            encoder=EmployeeUserVOListEncoder,
        )
    else:
        content = json.loads(request.body)

        employee_user = EmployeeUserVO.objects.create(**content)
        return JsonResponse(
            employee_user,
            encoder=EmployeeUserVOListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_automobiles(request, pk=None):
    if request.method == "GET":
        if pk is not None:
            automobile_vo = AutomobileVO.objects.filter(id=pk)
        else:
            automobile_vo = AutomobileVO.objects.all()

        return JsonResponse(
            {"automobiles": automobile_vo},
            encoder=AutomobileVODetailEncoder,
        )
    else:
        content = json.loads(request.body)

        automobile_vo = AutomobileVO.objects.create(**content)
        return JsonResponse(
            automobile_vo,
            encoder=AutomobileVODetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_customer(request, pk=None):
    if request.method == "GET":
        if pk is not None:
            customer = Customer.objects.filter(id=pk)
        else:
            customer = Customer.objects.all()

        return JsonResponse(
            {"customers": customer},
            encoder=CustomerListEncoder,
        )
    else:
        content = json.loads(request.body)

        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_detail_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            Customer.objects.filter(id=pk).update(**content)
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )
        

@require_http_methods(["GET", "POST"])
def api_list_sale_records(request, pk=None):
    if request.method == "GET":
        if pk is not None:
            sale_record = SalesRecord.objects.filter(id=pk)
            return JsonResponse(
            {"sales_records": sale_record},
            encoder=SalesRecordListEncoder,
        )
        else:
            sale_record = SalesRecord.objects.all()

        return JsonResponse(
            {"sales_records": sale_record},
            encoder=SalesRecordListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            vins = content["automobile"]
            auto = AutomobileVO.objects.get(vin=vins)
            auto.sold = True
            auto.save()
            content["automobile"] = auto
        
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Vin does not exist"},
                status=400,
            )

        try:
            employee_vo = content["employee_user"]
            employee = EmployeeUserVO.objects.get(employee_id=employee_vo)
            content["employee_user"] = employee
        
        except EmployeeUserVO.DoesNotExist:
            return JsonResponse(
                {"message": "Employee does not exist"},
                status=400,
            )

        try:
            returning_customer = content["customer"]
            customer = Customer.objects.get(id=returning_customer)
            content["customer"] = customer
        
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )

        sale_record = SalesRecord.objects.create(**content)
        return JsonResponse(
            sale_record,
            encoder=SalesRecordListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_detail_sale_record(request, pk):
    if request.method == "GET":
        sale_record = SalesRecord.objects.get(id=pk)
        return JsonResponse(
            sale_record,
            encoder=SalesRecordDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = SalesRecord.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            SalesRecord.objects.filter(id=pk).update(**content)
            sale_record = SalesRecord.objects.get(id=pk)
            return JsonResponse(
                sale_record,
                encoder=SalesRecordDetailEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )
