import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import AutomobileVO, EmployeeUserVO

def get_automobiles():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    content = json.loads(response.content)
    for automobile in content["autos"]:
        AutomobileVO.objects.update_or_create(
            href=automobile["href"],
            defaults={"vin": automobile["vin"], "model": automobile["model"]["name"]}
        )

def get_accounts():
    response = requests.get("http://accounts-api:8000/accounts/")
    content = json.loads(response.content)
    print("test---->", content)
    for accounts in content["employee_accounts"]:
        EmployeeUserVO.objects.update_or_create(
            employee_id=accounts["employee_id"],
            defaults={"first_name": accounts["first_name"],
            "last_name": accounts["last_name"],
            # "employee_id": accounts["employee_id"],
            "sale_employee": accounts["sale_employee"],
            "service_employee": accounts["service_employee"],
            }
        )


def poll():
    while True:
        print('Sales poller polling for data')
        try:
            get_automobiles(),
            get_accounts()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
