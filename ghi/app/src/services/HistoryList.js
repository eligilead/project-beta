import React from "react";





class HistoryList extends React.Component{
    state={
            services : [],
            vin: "",
        }


    handleVinChnage = (event) => {
        const value = event.target.value;
        this.setState({vin: value})
    }
    async loadservices() {
        const response = await fetch ('http://localhost:8080/api/services/');
        if (response.ok) {
            const data = await response.json();
            this.setState({services:data.services})
        } else {
            console.error(response);
        }
    }
    async search(event){
        event.preventDefault();
        let updatedservice = this.state.services.filter((service) => service.automobile.vin === this.state.vin);
        this.setState({services : updatedservice})
    }

    async componentDidMount() {
        this.loadservices()
    }
    render(){
        return(
            <div>
                <nav className="navbar bg-light">
                    <div className="container-fluid">
                        <form className="d-flex" role="search">
                        <input onChange={this.handleVinChnage} className="form-control me-2" type="search" placeholder="VIN" aria-label="Search"/>
                        <button onClick={(event)=> this.search(event)} className="btn btn-outline-success" type="submit">Search</button>
                        </form>
                    </div>
                </nav>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.services.map(service =>{
                            return(
                                <tr key={service.id}>
                                    <td>{ service.automobile.vin }</td>
                                    <td>{ service.customer.full_name }</td>
                                    <td>{ service.date }</td>
                                    <th>{ service.time }</th>
                                    <td>{ service.technician.name }</td>
                                    <td>{ service.reason }</td>
                                    <td>{ service.status }</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
          );
        }
}

export default HistoryList
