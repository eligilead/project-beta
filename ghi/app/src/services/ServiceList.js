
import React from 'react';


class ServiceList extends React.Component{
    state={
            services:[]
        }


    async loadservices() {
        const response = await fetch ('http://localhost:8080/api/services/');
        if (response.ok) {
            const data = await response.json();
            const filterdata = data.services.filter((service)=>service.status==="PENDING")
            this.setState({services:filterdata})

        } else {
            console.error(response);
        }
    }
    async finish(id){
        const data ={...this.state};
        await fetch(`http://localhost:8080/api/services/${id}/finish/`, {
                method: 'PUT',
                headers: {
                    'Accept' : 'application/json',
                    'Content-Type' : 'application/json'
                }
            })

                let updatedservice = this.state.services.filter(service => service.id !== id);
                this.setState({services : updatedservice})


      }
      async cancel(id){
        const data ={...this.state};
        await fetch(`http://localhost:8080/api/services/${id}/cancel/`, {
                method: 'PUT',
                headers: {
                    'Accept' : 'application/json',
                    'Content-Type' : 'application/json'
                }
            })

                let updatedservice = this.state.services.filter(service => service.id !== id);
                this.setState({services : updatedservice})


      }

    async componentDidMount() {
        this.loadservices()
    }
    render(){
        return(
          <table className="table table-striped ">
             <thead>
                 <tr>
                     <th>VIN</th>
                     <th>Customer name</th>
                     <th>VIP</th>
                     <th>Date</th>
                     <th>Time</th>
                     <th>Technician</th>
                     <th>Reason</th>
                     <th></th>
                     <th>Status</th>
                 </tr>
             </thead>
             <tbody>
                 {this.state.services.map(service =>{
                     return(
                         <tr key={service.id}>
                             <td>{ service.automobile.vin }</td>
                             <td>{ service.customer.full_name }</td>
                             <td>{ service.automobile.sold? "✔️":"❌" }</td>
                             <td>{ service.date }</td>
                             <th>{ service.time }</th>
                             <td>{ service.technician.name }</td>
                             <td>{ service.reason }</td>
                             <td>{ service.status }</td>
                             <td><button onClick={()=> this.finish(service.id)} type="button" className="btn btn-danger">Finish</button></td>
                             <td><button onClick={()=> this.cancel(service.id)} type="button" className="btn btn-danger">Cancel</button></td>
                         </tr>
                     )
                 })}
             </tbody>
         </table>
          );
        }
}


export default ServiceList;
