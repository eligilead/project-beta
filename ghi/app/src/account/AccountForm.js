import React from "react";

// change for Boolean
class AccountForm extends React.Component {
    state = {
        first_name: '',
        last_name: '',
        employee_id: '',
        sale_employee: false,
        service_employee: false,
        email: '',
    }
    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value })
    }
    handleBoolChange = (event) => {
        const name = event.target.name;
        const value = this.state[name]
        this.setState({ [name]: !value})
      }
    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state};


        const accountUrl = "http://localhost:8070/accounts/";
        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {'Content-Type': 'application/json'},
        };
        const response = await fetch(accountUrl, fetchConfig);
        if (response.ok) {
          const cleared = {
            first_name: '',
            last_name: '',
            employee_id: '',
            sale_employee: false,
            service_employee: false,
            email: '',
          };
          this.setState(cleared);
        }
        // else {
        //   const data = await response.json();
        //   alert(data.message)
        // }
      }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Employee</h1>
                        <form onSubmit={this.handleSubmit} id="create-employee-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.first_name} placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control" />
                                <label htmlFor="first_name">First name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.last_name} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control" />
                                <label htmlFor="last_name">Last name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.employee_id} placeholder="employeid" required type="number" name="employee_id" id="employee_id" className="form-control" />
                                <label htmlFor="employee_id">Employee Number</label>
                            </div>
                            <div className="form-check">
                                <input onChange={this.handleBoolChange} value={this.state.sale_employee} placeholder="sale_employee" type="checkbox" name="sale_employee" id="sale_employee" className="form-check-input"/>
                                <label htmlFor="sale_employee">sale employee</label>
                            </div>
                            <div className="form-check">
                                <input onChange={this.handleBoolChange} value={this.state.service_employee} placeholder="service_employee" type="checkbox" name="service_employee" id="service_employee" className="form-check-input"/>
                                <label htmlFor="service_employee">service employee</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.email} placeholder="email" required type="email" name="email" id="email" className="form-control" />
                                <label htmlFor="email">Email</label>
                            </div>
                            {/* <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
                            <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                            </div> */}
                            <div className="mb-3">
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}


export default AccountForm
