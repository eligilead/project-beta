import React from 'react';


class AccountList extends React.Component {
    state = {
        employee_accounts: []
    }
    async loadservices() {
        const response = await fetch('http://localhost:8070/accounts/');
        if (response.ok) {
            const data = await response.json();

            // const filterdata = data.services.filter((service)=>service.status==="PENDING")
            // this.setState({services:filterdata})
            this.setState({ employee_accounts: data.employee_accounts })


        } else {
            console.error(response);
        }
    }
    async componentDidMount() {
        this.loadservices()
    }
    render() {
        return (
            <div>

                <table className="table table-striped ">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>Name</th>
                        <th>Sales</th>
                        <th>Service</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.employee_accounts.map(account => {
                        return (
                            <tr key={account.id}>
                                <td>{account.employee_id}</td>
                                <td>{account.first_name }{" "}{account.last_name}</td>
                                <td>{account.sale_employee ? "✔️" :""}</td>
                                <td>{account.service_employee ? "✔️" : ""}</td>
                                {/* <td><button onClick={() => this.finish(service.id)} type="button" className="btn btn-danger">Finish</button></td>
                                <td><button onClick={() => this.cancel(service.id)} type="button" className="btn btn-danger">Cancel</button></td> */}
                            </tr>
                        )
                    })}
                </tbody>
                </table>

            </div>
        )
    }
}

export default AccountList
