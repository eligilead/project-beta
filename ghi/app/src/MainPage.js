import React from "react";

class MainPage extends React.Component {
  state = {
    Make_Name: [],
    Model_Name: [],
    Results: [],
    jeepMakes: [],
  };

  async loadservices() {
    const response = await fetch(
      "https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformake/jeep?format=json"
    );
    if (response.ok) {
      const data = await response.json();
      this.setState({ Results: data.Results });
      const jeepModelMakes = this.state.Results.map(
        (jeepMake) => jeepMake.Model_Name
      );
      this.setState({ jeepMakes: jeepModelMakes });
    }
  }

  async componentDidMount() {
    this.loadservices();
  }

  render() {
    return (
      <>
        <div
          id="carouselExampleControls"
          class="carousel slide"
          data-bs-ride="carousel"
        >
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img
                src="https://www.jeep.com/content/dam/fca-brands/na/jeep/en_us/2022/grand-cherokee-wl/desktop/2022-All-New-Grand-Cherokee-Overview-Hero-Desktop.jpg"
                class="d-block w-100"
                width="100%"
                height="450"
              />
            </div>
            <div class="carousel-item">
              <img
                src="https://di-uploads-pod15.dealerinspire.com/championcjdofindianapolis/uploads/2021/01/21Jeep-Cherokee-DrivingThroughStream2-21x9.png"
                class="d-block w-100"
                width="100%"
                height="450"
              />
            </div>
            <div class="carousel-item">
              <img
                src="https://www.auto-lifestyle.com/wp-content/uploads/2017/12/Jeep-Wrangler-2018_1.jpg"
                class="d-block w-100"
                width="100%"
                height="450"
              />
            </div>
            <div class="carousel-item">
              <img
                src="https://dealerinspire-image-library-test.s3.us-east-1.amazonaws.com/images/kJ8NNQM0k5CNHvo55wTprsr5zcnAUikjYdev9whb.png"
                class="d-block w-100"
                width="100%"
                height="450"
              />
            </div>
          </div>
          <button
            class="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleControls"
            data-bs-slide="prev"
          >
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button
            class="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleControls"
            data-bs-slide="next"
          >
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
        <div className="text-center">
          <h1 className="display-5 fw-bold">
          {/* <span class="d-block p-2 bg-dark text-white">d-block</span> */}

            <span class="d-block p-1 bg-dark text-white">E & J Authorized Jeep Dealer</span>
          </h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              Jeep is an American automobile marque, now owned by multi-national
              corporation Stellantis. Jeep has been part of Chrysler since 1987,
              when Chrysler acquired the Jeep brand, along with remaining
              assets, from its previous owner American Motors Corporation.
            </p>
          </div>
          {this.state.jeepMakes.map((jeep, index) => {
            if (index % 2 === 1) {
              return null;
            } else {
              return (
                <section id="gallery">
                  <div class="container">
                    <div class="row mb-5">
                      <div class="col">
                        <div class="card bg-dark text-white">
                          <img
                            src={`https://cdn.imagin.studio/getImage?customer=uselijahgileadcompany&make=jeep&modelFamily=${this.state.jeepMakes[index]}`}
                            class="card-img-top"
                          />
                          <div class="card-body">
                            <h5 class="card-title">{`Jeep ${this.state.jeepMakes[index]}`}</h5>
                            <p class="card-text ">
                              Lorem ipsum dolor sit amet consectetur,
                              adipisicing elit. Ut eum similique repellat a
                              laborum, repellendus praesentium quae!
                            </p>
                            <footer>
                              Buy the {this.state.jeepMakes[index]}
                            </footer>
                          </div>
                        </div>
                      </div>
                      <div class="col">
                        <div class="card bg-dark text-white">
                          <img
                            src={`https://cdn.imagin.studio/getImage?customer=uselijahgileadcompany&make=jeep&modelFamily=${
                              this.state.jeepMakes[index + 1]
                            }`}
                            class="card-img-top"
                          />
                          <div class="card-body">
                            <h5 class="card-title">{`Jeep ${
                              this.state.jeepMakes[index + 1]
                            }`}</h5>
                            <p class="card-text ">
                              Lorem ipsum dolor sit amet consectetur,
                              adipisicing elit. Ut eum similique repellat a
                              laborum, repellendus praesentium quae!
                            </p>
                            <footer>
                              Buy the {this.state.jeepMakes[index + 1]}
                            </footer>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              );
            }
          })}
          <br />
          <footer className="lead mb-4">
          <a href="https://www.linkedin.com/in/eli-gilead/">Elijah Gilead</a>  |  <a href="https://www.linkedin.com/in/jonas-pf/">Jonas Petit-Frere</a>
          </footer>
        </div>
      </>
    );
  }
}

export default MainPage;
