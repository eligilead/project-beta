import React from 'react';


class AutomobileList extends React.Component {
    state = {autos: []};

    async componentDidMount(props) {

      const url = "http://localhost:8100/api/automobiles/";
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        this.setState({autos:data.autos});
      }
    }
    render() {

      return (
        <>
        <h1>Vehicle models</h1>
        <table className="table table-striped">
          <thead>
              <tr>
                  <th>Vin</th>
                  <th>Color</th>
                  <th>Year</th>
                  <th>Model</th>
                  <th>Manufacturer</th>
              </tr>
          </thead>
          <tbody>
              {this.state.autos.map(vehicle =>{
                  return(
                      <tr key={vehicle.id}>
                          <td>{ vehicle.vin }</td>
                          <td>{ vehicle.color }</td>
                          <td>{ vehicle.year }</td>
                          <td>{ vehicle.model.name }</td>
                          <td>{ vehicle.model.manufacturer.name }</td>
                      </tr>
                  )
              })}
          </tbody>
        </table>
        </>
      );
    }
    }

export default AutomobileList;
