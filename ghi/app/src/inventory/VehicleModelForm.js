import React from "react";


class VehicleModelForm extends React.Component{
        state = {
            name: '',
            picture_url: '',
            manufacturers: [],
            manufacturer_id:'',
        }


    handleNameChange = (event) => {
        const value = event.target.value;
        this.setState({name: value})
    }
    handlePictureUrlChange = (event) => {
        const value = event.target.value;
        this.setState({picture_url: value})
    }
    handleManufacturerChange = (event) => {
        const value = event.target.value;
        this.setState({manufacturer_id: value})
    }
    handleSubmit = async (event)  => {
        event.preventDefault();
        const data= {...this.state};
        delete data.manufacturers;

        const serviceUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {'Content-type': 'application/json'},
        };
        const response = await fetch(serviceUrl, fetchConfig);
        if (response.ok) {
            const newvehiclemodel = await response.json();

            const cleared = {
                name: '',
                picture_url: '',
                manufacturer:'',
                manufacturer_id:'',
            };
            this.setState(cleared);
          }
    }
    async componentDidMount() {
        const Url = "http://localhost:8100/api/manufacturers/";
        const Response = await fetch(Url);
        if( Response.ok){
            const data = await Response.json()
            this.setState({manufacturers:data.manufacturers})

        };
    }
    render(){
        return(
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a vehicle model</h1>
                <form onSubmit={this.handleSubmit} id="create-model-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="name" required type="name" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handlePictureUrlChange} value={this.state.picture_url} placeholder="picture_url" required type="picture_url" name="picture_url" id="picture_url" className="form-control"/>
                    <label htmlFor="picture_url">Picture url</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleManufacturerChange} value={this.state.manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                      <option value="">Choose a manufacturer</option>
                      {this.state.manufacturers.map(manufacturer =>{
                        return(
                          <option key={manufacturer.id} value={manufacturer.id}>
                            {manufacturer.name}
                          </option>
                        )
                      } )}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        )
    }
}
export default VehicleModelForm;
