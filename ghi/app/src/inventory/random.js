import React from "react";



class ApiList extends React.Component{
    state = {
        Make_Name: [],
        Model_Name: [],
        Results: [],
        }


    async loadservices(){
        const response = await fetch ('https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformake/toyota?format=json');
        if (response.ok) {
            const data = await response.json();
            this.setState({Results:data.Results})
            // console.log(this.state)

            // console.log("test---->", data)
            console.log("test---->", this.state.Results )

        } else {
            console.error(response);
        }
    }

    async componentDidMount(){
        this.loadservices()
    }
    render(){
        return(
            <table className="table table-striped">
            <thead>
                <tr>
                    <th>model-name</th>
                    {/* <th>Manufacturer</th>
                    <th>Picture</th>
                    <th>stuff</th> */}
                </tr>
            </thead>
            <tbody>
                {this.state.Results.map(api_info =>{
                    return(
                        <tr key={api_info.Model_ID}>
                            <td>{ api_info.Make_Name } { api_info.Model_Name }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        
        )
    }
}

export default ApiList
