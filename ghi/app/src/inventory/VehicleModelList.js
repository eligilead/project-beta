import React from "react";



class VehicleModelList extends React.Component{
    state= {
            models: []
        }


    async loadservices(){
        const response = await fetch ('http://localhost:8100/api/models/');
        if (response.ok) {
            const data = await response.json();
            this.setState({models:data.models})
        } else {
            console.error(response);
        }
    }

    async componentDidMount(){
        this.loadservices()
    }
    render(){
        return(
            <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {this.state.models.map(model =>{
                    return(
                        <tr key={model.id}>
                            <td>{ model.name }</td>
                            <td>{ model.manufacturer.name }</td>
                            <td> <img src={ model.picture_url }width= "400" height= "200"/></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        )
    }
}

export default VehicleModelList
