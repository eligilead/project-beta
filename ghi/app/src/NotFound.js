import { Link } from "react-router-dom";

function NotFound() {
    return (
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">CarCar</h1>
        <div className="col-lg-6 mx-auto">
        <img src="https://media0.giphy.com/media/MSU9sITGoHWMGGVn9n/giphy.gif"/>

        <footer>
        <button className="btn btn-outline-primary" type="button"><Link to="/"> Return Home </Link></button>
        </footer>

        
        </div>
      </div>
    );
  }
  
  export default NotFound;
