import React from "react";

class IndividualSaleList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      employee_users: [],
      sales_records: [],
      employee_user: "",
    };
    this.handleOptionChange = this.handleOptionChange.bind(this);
  }

  async componentDidMount(props) {
    const saleRecordUrl = "http://localhost:8090/api/sales-records/";
    const saleRecordUrlResponse = await fetch(saleRecordUrl);
    if (saleRecordUrlResponse.ok) {
      const data = await saleRecordUrlResponse.json();
      this.setState({ sales_records: data.sales_records });
    }

    const salesPersonsUrl = "http://localhost:8090/api/employees/";
    const salesPersonsUrlResponse = await fetch(salesPersonsUrl);
    if (salesPersonsUrlResponse.ok) {
      const data = await salesPersonsUrlResponse.json();
      this.setState({ employee_users: data.employee_users });
    }
  }

  handleOptionChange(event) {
    const value = event.target.value;
    this.setState({ employee_user: value });
    console.log(this.state.employee_user);

    // this how you access the value
    // value is connected to option is child to select tag, thats the connecting
    // onChange is on slecet just because, if on options boom
  }

  render() {
    let applicable_records = this.state.sales_records;
    if (this.state.employee_user !== "") {
      applicable_records = this.state.sales_records.filter(
        (sales_record) =>
          sales_record.employee_id === parseInt(this.state.employee_user)
      );
      // applicable_records is all the info on the sales_records api request
      // this starts all emepty until clicked
      // upon clicking select tag(think select/ option tag below) value clicked in
      // option tag stores that data in a string
      // filter will remove all thing that dont match
    }
    return (
      <div>
        <h1>Sales person history</h1>
        <select
          onChange={this.handleOptionChange}
          required
          name="dynamic_dropdown"
          id="dynamic_dropdown"
          className="form-select"
        >
          <option value=""></option>
          {this.state.employee_users.map((employee) => {
            if (employee.service_employee === true) {
              return null;
            } else {
              return (
                <option key={employee.employee_id} value={employee.employee_id}>
                  {employee.first_name}
                </option>
              );
            }
          })}
        </select>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales Rep</th>
              <th>Customer</th>
              <th>Vin</th>
              <th>Sale Price</th>
            </tr>
          </thead>
          <tbody>
            {applicable_records.map((sale_info) => {
              return (
                <tr key={sale_info.id}>
                  <td>{sale_info.employee_user}</td>
                  <td>{sale_info.customer}</td>
                  <td>{sale_info.automobile}</td>
                  <td>{sale_info.sales_price}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default IndividualSaleList;

{
  /* <option value="">a</option>sa */
}
{
  /* render options of selected, try to connect to below */
}
{
  /* onChange, hand in value in state, that determines what displays (rerender)*/
}
{
  /*  */
}
