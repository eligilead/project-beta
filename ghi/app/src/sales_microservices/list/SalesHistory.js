import React from "react";

class SalesList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sales_records: [],
    };
  }

  async componentDidMount() {
    const url = "http://localhost:8090/api/sales-records/";

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        await this.setState({ sales_records: data.sales_records });
        console.log("test---->", data);
      }
    } catch (e) {
      window.alert(e);
    }
  }

  render() {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Sales Rep</th>
            <th>Employee id</th>
            <th>Customer</th>
            <th>Vin</th>
            <th>Sale Price</th>
          </tr>
        </thead>
        <tbody>
          {this.state.sales_records.map((sale_info) => {
            return (
              <tr key={sale_info.id}>
                <td>{sale_info.employee_user}</td>
                <td>{sale_info.employee_id}</td>
                <td>{sale_info.customer}</td>
                <td>{sale_info.automobile}</td>
                <td>{sale_info.sales_price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}

export default SalesList;
