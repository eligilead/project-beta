import React from "react";


class SalePersonForm extends React.Component{
  state ={full_name: '', employee_id: ''};


  handleFullNameChange = (e) => {
    const value= e.target.value;
    this.setState({full_name: value})
  }
  handleEmployeeIdChange = (e) => {
    const value= e.target.value;
    this.setState({employee_id: value})
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    const data ={...this.state};

    const salesRepUrl = "http://localhost:8090/api/sales-rep/"
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(salesRepUrl, fetchConfig)
    if (response.ok) {
      const newSalesRep = await response.json();

      const cleared ={
        full_name: '',
        employee_id: '',

      };
      this.setState(cleared)
    }
  }


    render(){
        return(
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create Sales Person</h1>
                <form onSubmit={this.handleSubmit} id="create-customer-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleFullNameChange} value={this.state.full_name} placeholder="full_name" required type="text" name="full_name" id="full_name" className="form-control"/>
                    <label htmlFor="full_name">Full Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleEmployeeIdChange} value={this.state.employee_id} placeholder="employee_id" required type="text" name="employee_id" id="employee_id" className="form-control"/>
                    <label htmlFor="employee_id">Employee id</label>
                  </div>
                <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
    }



export default SalePersonForm;
