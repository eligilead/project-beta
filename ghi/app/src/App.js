import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerForm from './sales_microservices/creation_forms/CustomerForm';
import SalePersonForm from './sales_microservices/creation_forms/SalesPersonForm';
import SaleRecordForm from './sales_microservices/creation_forms/CreateSaleRecord';
import SalesList from './sales_microservices/list/SalesHistory';
import IndividualSaleList from './sales_microservices/list/IndivdualSalesRecord';
import TechnicianForm from './services/TechnicianForm';
import ServiceForm from './services/ServiceForm';
import ServiceList from './services/ServiceList';
import HistoryList from './services/HistoryList';
import ManufacturerList from './inventory/ManufacturersList';
import ManufacturerForm from './inventory/ManufacturerForm';
import AutomobileList from './inventory/AutomobilesList';
import VehicleModelList from './inventory/VehicleModelList';
import VehicleModelForm from './inventory/VehicleModelForm';
import AutomobileForm from './inventory/AutomobileForm';
import NotFound from './NotFound';
import ApiList from './inventory/random';
import AccountList from './account/AccountList';
import AccountForm from './account/AccountForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="create">
            <Route path="customer" element={<CustomerForm />} />
            <Route path="sale-person" element={<SalePersonForm />} />
            <Route path="sale-record" element={<SaleRecordForm />} />
            <Route path="manufacturer" element={<ManufacturerForm  />} />
            <Route path="automobile" element={<AutomobileForm  />} />
            <Route path="model" element={<VehicleModelForm/>} />
            <Route path="service" element={<ServiceForm />} />
            <Route path="technician" element={<TechnicianForm />} />
          </Route>


          <Route path="list">
            <Route path="sales" element={<SalesList />} />
            <Route path="individual" element={<IndividualSaleList />} />
            <Route path="manufacturer" element={<ManufacturerList />} />
            <Route path="automobiles" element={<AutomobileList />} />
            <Route path="service-history" element={<HistoryList />} />
            <Route path="services" element={<ServiceList />} />
            <Route path="models" element={<VehicleModelList/>} />
            <Route path="api" element={<ApiList/>} />
          </Route>

          <Route path="*" element={<NotFound/>}> </Route>
          <Route path="accounts" element={<AccountList/> } />
          <Route path="accounts/new" element={<AccountForm/>} />



        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
